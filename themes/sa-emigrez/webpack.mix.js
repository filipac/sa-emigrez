const mix = require('laravel-mix');
require('laravel-mix-tailwind');

// mix.js('assets/js/app.js', 'public/js')
   mix
   .sass('assets/sass/app.scss', 'assets/css')
   .tailwind();
